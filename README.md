## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_hcloud"></a> [hcloud](#requirement\_hcloud) | 1.32.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_hcloud"></a> [hcloud](#provider\_hcloud) | 1.32.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [hcloud_network.vpc](https://registry.terraform.io/providers/hetznercloud/hcloud/1.32.1/docs/resources/network) | resource |
| [hcloud_network_subnet.subnets](https://registry.terraform.io/providers/hetznercloud/hcloud/1.32.1/docs/resources/network_subnet) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cidr"></a> [cidr](#input\_cidr) | n/a | `string` | `"10.10.0.0/16"` | no |
| <a name="input_name"></a> [name](#input\_name) | n/a | `any` | n/a | yes |
| <a name="input_subnets"></a> [subnets](#input\_subnets) | n/a | `list` | <pre>[<br>  {<br>    "cidr": "10.10.1.0/24",<br>    "name": "subnet-1",<br>    "region": "eu-central"<br>  },<br>  {<br>    "cidr": "10.10.2.0/24",<br>    "name": "subnet-2",<br>    "region": "eu-central"<br>  }<br>]</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_subnets"></a> [subnets](#output\_subnets) | n/a |
| <a name="output_vpc"></a> [vpc](#output\_vpc) | n/a |
