variable "cidr" {
  default = "10.10.0.0/16"
}
variable "name" {}

variable "subnets" {
  # type = list(map(string))
  default = [
    {
      name   = "subnet-1"
      region = "eu-central"
      cidr   = "10.10.1.0/24"
    },
    {
      name   = "subnet-2"
      region = "eu-central"
      cidr   = "10.10.2.0/24"
    },
  ]
}