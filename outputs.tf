output "vpc" {
  value = hcloud_network.vpc
}
output "subnets" {
  value = hcloud_network_subnet.subnets
}