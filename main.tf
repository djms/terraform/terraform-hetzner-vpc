resource "hcloud_network" "vpc" {
  name     = var.name
  ip_range = var.cidr
}
resource "hcloud_network_subnet" "subnets" {
  count        = length(var.subnets)
  network_id   = hcloud_network.vpc.id
  type         = "cloud"
  network_zone = var.subnets[count.index].region
  ip_range     = var.subnets[count.index].cidr
}
